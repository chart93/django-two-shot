from django.forms import ModelForm
from receipts.models import Receipt
from django.db import models
from django.conf import settings


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            'vendor',
            'total',
            'tax',
            'date',
            'category',
            'account',
        )
